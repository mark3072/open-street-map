<?php
header("content-type: text/html; charset=UTF-8");
?>

<head>
   <link href="css/common.css" rel="stylesheet" type="text/css" />
   <script>
   function generateRandomCPU() {
      document.getElementById("CPU").innerHTML = (Math.random()*100).toFixed(0);
      document.getElementById("CPU").innerHTML += "%"
   }
   function generateRandomMem() {
      document.getElementById("RAM").innerHTML = (Math.random() * (32 - 1) + 1).toFixed(1);
      document.getElementById("RAM").innerHTML += " / 32GB";
   }
   function generateRandoms() {
      generateRandomCPU();
      generateRandomMem();
   }
   </script>
</head>

<body id="debug page" onload="generateRandoms()">
<!--
   <h1>Debug page</h3>
   <div>
   <span>server ip: </span>
   <span>52.90.242.24:8080</span>
   </div>


   <div id="content">
   <h3>System stats</h3>

   <div>
   <span class="label">CPU Usage: </span>
   <span class="value" id="CPU">50%</span>
   </div>

   <div>
   <span class="label">RAM Usage: </span>
   <span class="value" id="RAM">2.1GB / 32GB</span>
   </div>

   <h3>PSQL Stats</h3>
   <div>
   <span class="label">Last SQL Statement: </span>
   <span class="value" id="LastSQL">SELECT * FROM place_id</span>
   </div>

   <div>
   <span class="label">Current number of connections: </span>
   <span class="value" id="TPS">73</span>
   </div>

   <div>
   <span class="label">Transactions per second: </span>
   <span class="value" id="TPS">1025</span>
   </div>

    </div> <!-- /content -->

Not implemented


</body>
</html>
