# Nominatim Server Code
This repository holds the php files that are served by the Nominatim webserver to provide speed limit lookups. 
There is an install script created to copy the files from this repo to the appropriate location on the EC2 instance. 

## Nominatim
The [Nominatim](https://nominatim.org/) server is installed with the nominatim user. The user's home directory is set to `/srv/nominatim`, 
with the server sitting in `/srv/nominatim/Nominatim`. 

`/srv/nominatim/Nominatim/build/website` is the root of the webserver, and contains the available endpoints. The simply reference
files in `/srv/nominatim/Nominatim/website`. 

`/srv/nominatim/Nominatim/website` contain php files with the actual logic. They reference files in `/srv/nominatim/Nominatim/lib` to query the database.

There is a postgresql database that has a `nominatim` user with a `nominatim` database associated with it. Along with this database, there is a simple file
`/srv/nominatim/nominatim.file` that contains node coordinates. This file is ~53GB for North America alone, and contains the majority of the OSM data. 

The php files contained in this repo are additional endpoints used by AWS ([healthcheck.php](./healthcheck.php)) and for direct speed limit lookup ([maxspeed.php](./maxspeed.php)). The [install script](./install)
is responsible for copying the files to the correct location on EC2. Particularly, the files in the root of the repo should go to `/srv/nominatim/Nominatim/website`
and the ones in [build/website](./build/website) should go to `/srv/nominatim/Nominatim/build/website`.

## Files on EC2
There are a few files contained in the EC2 image in AWS (copies of these files are in the 'ec2-image-files' dir)

**/lib/systemd/system/nominatim.service**

This file adds `nominatim` to systemd, and allows the php files to be installed into the webserver using systemd.
`sudo systemctl enable nominatim` : Enables the start script to run automatically when the server starts up. This has been enabled on the server image
`sudo systemctl restart nominatim` : Re-runs the start script to pull and install the latest php code

**/lib/systemd/system/nominatim-update.service**

This file adds `nominatim-update` to systemd, and is a continuously running process to keep the server up to date. 
May 6, 2022
Using a daily north american feed to update the DB

`sudo systemctl enable nominatim-update` : Enables the start script to run automatically when the server starts up. This has been enabled on the server image
`sudo systemctl restart nominatim` : Re-starts the update process

**/srv/nominatim/start**

Run when nominatim is started via systemd. If this repo exists, checkout master and attempt to pull the latest code, otherwise clone the repository. 
Then run the install script contained in this directory. Any actions that need to be perfomed on startup should be in the install script


## On the server
In the server, systemd is controlled by the ubuntu user (e.g. sudo systemctl ...). However the nominatim files and service is owned
by the nominatim user. You can change users with: `sudo su - nominatim`. 

The home folder for the nominatim user is /srv/nominatim

After switching to the nominatim user, you can navigate to the project dir: ~/nominatim-project
From here, you can run the CLI tool to manage nominatim. 

`nominatim --help` for details on this tool

Updating OSM DB and enabling Autoupdates
Spun up a test server using the OSM-Server AMI to test the process
Based on INFRA-537 I checked the following urls to see their output to verify if the update 
was successful

http://100.26.5.180:8080/maxspeed.php?locs=[{%22lat%22:26.2296027,%22lon%22:-81.6330772}]
{{{ output 
[{"lat":26.2296027,"lon":-81.6330772,"place_id":17531888, "maxspeed":"55"}]
}}}
http://100.26.5.180:8080/reverse.php?lat=26.2296027&lon=-81.6330772&zoom=16
{{{ output
<reversegeocode timestamp="Mon, 02 May 22 23:59:03 +0000" attribution="Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright" querystring="lat=26.2296027&lon=-81.6330772&zoom=16"> <result place_id="17531888" osm_type="way" osm_id="10852510" ref="Golden Gate Boulevard West" lat="26.2295784148087" lon="-81.6330769081555" boundingbox="26.229115,26.2298739,-81.6714321,-81.606665" place_rank="26" address_rank="26">Golden Gate Boulevard West, Golden Gate Estates, Collier County, Florida, 34120, United States</result> <addressparts> <road>Golden Gate Boulevard West</road> <residential>Golden Gate Estates</residential> <county>Collier County</county> <state>Florida</state> <postcode>34120</postcode> <country>United States</country> <country_code>us</country_code> </addressparts> </reversegeocode>
}}}
http://100.26.5.180:8080/details.php?place_id=17531888&osmtype=N
{{{ output
{"place_id":17531888,"parent_place_id":52657240,"osm_type":"W","osm_id":10852510,"category":"highway","type":"secondary","admin_level":15,"localname":"Golden Gate Boulevard West","names":{"name":"Golden Gate Boulevard West"},"addresstags":{"postcode":"34120","tiger:county":"Collier county"},"housenumber":null,"calculated_postcode":"34120","country_code":"us","indexed_date":"2020-12-18T00:36:44+00:00","importance":0,"calculated_importance":0.1,"extratags":{"lanes":"2","maxspeed":"55","oneway":"yes"},"calculated_wikipedia":null,"rank_address":26,"rank_search":26,"isarea":false,"centroid":{"type":"Point","coordinates":[-81.6392288,26.2295047]},"geometry":{"type":"Point","coordinates":[-81.6392288,26.2295047]}}
}}}

The latest data for this lat lon has a maxspeed of 45 mph
https://nominatim.openstreetmap.org/details.php?osmtype=W&osmid=814029007&class=highway&addressdetails=1&hierarchy=0&group_hierarchy=1&format=json
{{{ output
{"place_id":258442939,"parent_place_id":283187808,"osm_type":"W","osm_id":814029007,"category":"highway","type":"secondary","admin_level":15,"localname":"Golden Gate Boulevard West","names":{"name":"Golden Gate Boulevard West"},"addresstags":[],"housenumber":null,"calculated_postcode":"34119-9603","country_code":"us","indexed_date":"2021-10-25T21:52:10+00:00","importance":0,"calculated_importance":0.09999999999999998,"extratags":{"bicycle":"yes","cycleway:right":"lane","lanes":"2","lit":"no","maxspeed":"45 mph","oneway":"yes","surface":"asphalt"},"calculated_wikipedia":null,"rank_address":26,"rank_search":26,"isarea":false,"centroid":{"type":"Point","coordinates":[-81.6382227,26.2295164]},"geometry":{"type":"Point","coordinates":[-81.6382227,26.2295164]},"address":[{"localname":"Golden Gate Boulevard West","place_id":258442939,"osm_id":814029007,"osm_type":"W","place_type":null,"class":"highway","type":"secondary","admin_level":15,"rank_address":26,"distance":0,"isaddress":true},{"localname":"Golden Gate Estates","place_id":283187808,"osm_id":6612640,"osm_type":"R","place_type":null,"class":"landuse","type":"residential","admin_level":15,"rank_address":22,"distance":0.06045656985301953,"isaddress":true},{"localname":"Collier County","place_id":282599437,"osm_id":1210705,"osm_type":"R","place_type":"county","class":"boundary","type":"administrative","admin_level":6,"rank_address":12,"distance":0.27991475900475843,"isaddress":true},{"localname":"Florida","place_id":282323762,"osm_id":162050,"osm_type":"R","place_type":"state","class":"boundary","type":"administrative","admin_level":4,"rank_address":8,"distance":1.535102126549204,"isaddress":true},{"localname":"34119-9603","place_id":null,"osm_id":null,"osm_type":null,"place_type":null,"class":"place","type":"postcode","admin_level":null,"rank_address":5,"distance":0,"isaddress":true},{"localname":"United States","place_id":null,"osm_id":null,"osm_type":null,"place_type":null,"class":"place","type":"country","admin_level":null,"rank_address":4,"distance":0,"isaddress":true},{"localname":"us","place_id":null,"osm_id":null,"osm_type":null,"place_type":null,"class":"place","type":"country_code","admin_level":null,"rank_address":4,"distance":0,"isaddress":false}]}
}}}


Followed steps here to update: https://nominatim.org/release-docs/latest/admin/Update/
Switched to nominatim user
```
sudo su - nominatim
```

Installed Pyosmium
```
pip3 install --user osmium
```



May 2, 2022
Updating Nominatim from 3.6.0 -> 3.7.0 and then to latest (4.0.1)
https://nominatim.org/release-docs/latest/admin/Migration/

Cloned 3.7.0 repo
git clone --recursive https://github.com/openstreetmap/Nominatim.git Nominatim-3.7.0

Installed country grid
wget -O Nominatim-3.7.0/data/country_osm_grid.sql.gz https://www.nominatim.org/data/country_grid.sql.gz

Created build dir in Nominatim-3.7.0
cd Nominatim-3.7.0
mkdir build
cd build

Build
cmake ..
make
sudo make install

cd /srv/nominatim/Nominatim-3.7.0
nominatim admin --migrate

