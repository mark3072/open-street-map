<?php

@define('CONST_Debug', $_GET['debug'] ?? false);
@define('CONST_LibDir', '/usr/local/lib/nominatim/lib-php');
@define('CONST_TokenizerDir', '/srv/nominatim/nominatim-project/tokenizer');
@define('CONST_NominatimVersion', '4.0.1-0');
@define('CONST_Database_DSN', 'pgsql:dbname=nominatim');
ini_set('memory_limit', '200M');

require_once(CONST_LibDir.'/init.php');
require_once(CONST_LibDir.'/ParameterParser.php');

$oParams = new Nominatim\ParameterParser();
$locs = json_decode($oParams->getString('locs'));

$oDB = new Nominatim\DB(CONST_Database_DSN);
$oDB->connect();


function getMaxspeedQuery($fLat, $fLon)
{
	// uses similar query to first query in lookupPoint(),
	// but returns maxspeed directly
	$sPoint = 'ST_SetSRID(ST_Point('.$fLon.','.$fLat.'),4326)';

	$fSearchDiam = 0.006;
	$aPlace = null;

	// for POI or street level
	$sSQL =  'SELECT place_id, \''.$fLat.'\' as lat, \''.$fLon.'\' as lon,	extratags->\'maxspeed\' as maxspeed, ';
	$sSQL .= 'ST_distance('.$sPoint.', geometry) as distance ';
	$sSQL .= 'FROM ';
	$sSQL .= 'placex ';
	$sSQL .= 'WHERE ST_DWithin('.$sPoint.', geometry, '.$fSearchDiam.') ';
	$sSQL .= 'AND ';
	$sSQL .= 'rank_address between 26 and 27 ';
	$sSQL .= 'ORDER BY distance ASC limit 1';
	return $sSQL;
}

$sFullQuery = null;
$result="";
if ($locs == null) {
	$result.="Bad request";
} else {
	foreach($locs as $loc) {
		// Build the query with UNIONS in between
		$sFullQuery .= "(".getMaxspeedQuery($loc->lat,$loc->lon).")";
		$sFullQuery .= " UNION ";
	}
	if (strlen($sFullQuery) > 1) {
		$sFullQuery = substr($sFullQuery, 0,-7);
	}
	$aRows = $oDB->getAll($sFullQuery, null, 'Could not determine closest place.');

	$result.="[";
	foreach($aRows as $row) {
		$iPlaceID = $row['place_id'];
		$fMaxspeed = $row['maxspeed'];
		$lat = $row['lat'];
		$lon = $row['lon'];

		$result.="{";
		$result.="\"lat\":";
		$result.=$lat;
		$result.=",";
		$result.="\"lon\":";
		$result.=$lon;
		$result.=",";
		$result.="\"place_id\":";
		$result.=$iPlaceID;
		$result.=", \"maxspeed\":\"";
		$result.=$fMaxspeed;
		$result.="\"}";
		$result.=",";
	}
	if (strlen($result) > 1) {
		$result = substr($result, 0,-1);
	}
	$result.="]";
}
print $result;
