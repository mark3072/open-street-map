from locust import HttpLocust, TaskSet, task, between, constant_pacing, constant
# from locust.contrib.fasthttp import FastHttpLocust

from random import seed
from random import randint
import re
import os

import json
import csv

seed(1)

filename = "./locust_files/warmup_locs.csv"
lookups = []
# Helpful regex to pull out the lookups for the vehicle row
regex = re.compile('\d+-\d+-\d+\s\d+:\d+:\d+,\d+,\d+,\"xxxxx\s(.*)\"')

# convert all the rows first
with open(filename) as f:
   f.readline()
   for row in f:
      regex.match(row)
      match = regex.match(row)
      # before we can parse to json, there are two double quotes we need to convert to just one
      matchStr = match.group(1)
      sanitizedStr = matchStr.replace('""','"')
      # Now we can parse
      parsedJson = json.loads(sanitizedStr)
      lookups.append(parsedJson['lookups'])

class ReverseGeocode(TaskSet):
   global lookups

   @task()
   def reverse(self):
      # Pick a random row in the lookups
      # Then build the query from the elements in that row
      queryJson = []

      rowNum = randint(0, 9998)
      row = lookups[rowNum]
      while len(row) == 0:
         rowNum = randint(0, 9998)
         row = lookups[rowNum]

      for loc in row:
         lat = loc['lat']
         lon = loc['lng']
         queryJson.append({'lat':lat, 'lon':lon})

      query = "/maxspeed.php?locs=%s" % json.dumps(queryJson)
      # print(query)
      resp = self.client.get(query, name="/maxspeed")
      if resp.text == "Bad request":
         print("Bad request")

class WebsiteUser(HttpLocust):
   host = "http://localhost:8080"
   task_set = ReverseGeocode
   wait_time = constant(0)


